import React, { useState, useEffect } from "react";


function SalesList() {

    const [allSales, setAllSales] = useState([]);

    async function loadAllSales() {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            setAllSales(data.sales);
        } else {
            console.error("Error getting sales list")
        }
    }

    useEffect(() => {
        loadAllSales();
    }, []);

    return (
        <>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Salesperson Employee ID</th>
                    <th>Salesperson Name</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {allSales?.map(sales => {
                    return (
                        <tr key={sales.id}>
                            <td>{sales.salesperson.employee_id}</td>
                            <td>{sales.salesperson.first_name} {sales.salesperson.last_name}</td>
                            <td>{`${sales.customer.first_name} ${sales.customer.last_name}`}</td>
                            <td>{`${sales.automobile.vin}`}</td>
                            <td>${sales.price}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    );
}

export default SalesList;
