import { useEffect, useState } from "react"


function AppointmentsHistory() {

    const [appointments, setAppointments] = useState([])
    const [searchBar, setSearchBar] = useState('')
     const [inventoryVins, setinventoryVins] = useState([])

    async function getData() {
        const response = await fetch('http://localhost:8080/api/appointments/')
        if (response.ok) {
            const data = await response.json()
            setAppointments(data.appointments)
        }
    }

    useEffect(() => {
        getData()
        getAutomobiles()
    }, [])

    const handleSearchBar = (e) => {
        const value = e.target.value
        setSearchBar(value)
    }

    useEffect(() => {

        getData()
    }, [])

     async function getAutomobiles() {
        const response = await fetch('http://localhost:8100/api/automobiles/')
        if (response.ok) {
            let inventoryVinsTemp = []

            const data = await response.json()

            for (let auto of data.autos) {
                inventoryVinsTemp.push(auto.vin)
            }
            setinventoryVins(inventoryVinsTemp)
        }
    }


    function isVip(appointmentVin) {
        const sold = inventoryVins.find(invenVin => invenVin == appointmentVin)

        if (appointmentVin === sold) {
            return "Yes"
        } else {
            return "No"
        }
    }


    return (
        <div>
            <h1>Service History</h1>

            <input
                type="text"
                className="input"
                placeholder="Search by VIN..."
                onChange={handleSearchBar}
                value = {searchBar}
            />
            <button onClick={handleSearchBar}>Search</button>

        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {appointments.map(appointment => {
                    if (appointment.vin.includes(searchBar)) {
                    return (
                        <tr key={appointment.id}>
                            <td>{ appointment.vin }</td>
                            <td>{ isVip(appointment.vin) }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ new Date(appointment.date_time).toLocaleDateString() }</td>
                            <td>{ new Date(appointment.date_time).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' }) }</td>
                            <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                            <td>{ appointment.reason }</td>
                            <td>{ appointment.status }</td>

                        </tr>
                    )};
                })}
            </tbody>
            </table>
        </div>
    );
}

export default AppointmentsHistory;
